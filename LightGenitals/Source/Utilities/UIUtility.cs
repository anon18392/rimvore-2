﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace LightGenitals
{
    public static class UIUtility
    {
        public static Func<float, string> percentagePresenter = delegate (float value) {
            if (value == 0) return "RV2_Settings_Disabled".Translate();
            else if (value == 1) return "RV2_Settings_Guaranteed".Translate();
            return Math.Round(value * 100).ToString() + "%";
        };
        public static float DoLabelledSlider(this Listing_Standard list, float currentValue, float minValue = 0, float maxValue = 9999, string label = null, string labelTooltip = null, Func<float, string> valuePresenter = null)
        {
            label += ": ";
            if (valuePresenter == null)
            {
                label += currentValue;
            }
            else
            {
                label += valuePresenter(currentValue);
            }
            list.Label(label, -1, labelTooltip);
            return list.Slider(currentValue, minValue, maxValue);
        }
    }
}
