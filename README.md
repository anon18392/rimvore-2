# Introduction
This is the repo for the mod RimVore2 for the game Rimworld by Ludeon Studios.

We are currently in early development and thus offer no support if you install this mod and break your game.

The mod contains extreme fetish content, based on Vore. If you dislike the fetish or things associated with it, **do not install this mod**.

You can join our discord server for direct discussions and troubleshooting: https://discord.gg/w9tYgTGgx4

If you find a bug or have a mod integration / compatibility request, [provide a ticket](https://gitlab.com/Nabber/rimvore-2/-/issues) and the development team will take a look at it.