﻿using HarmonyLib;
using HugsLib.Settings;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{


    public class RV2Mod : Mod
    {
        public static RV2Settings Settings;
        public static RV2Mod Mod;
        public RV2Mod(ModContentPack content) : base(content)
        {
            Settings = GetSettings<RV2Settings>();  // create !static! settings
            Mod = this;

            InitializeEarlyPatches();
        }

        static bool earlyPatchesInitialized = false;
        public static void InitializeEarlyPatches()
        {
            if(earlyPatchesInitialized)
                return;
            earlyPatchesInitialized = true;
            Harmony harmony = new Harmony("RV2_EARLY");

            // early patch to actually influence XML loading
            harmony.Patch(AccessTools.Method(typeof(DirectXmlLoader), "DefFromNode"), new HarmonyMethod(typeof(Patch_DirectXmlLoader), "InterruptDateTaggedNodes"));
        }

        public void DefsLoaded()
        {
            Settings.DefsLoaded();
        }

        public override string SettingsCategory()
        {
            return "RV2_Settings_Category".Translate();
        }

        /// <summary>
        /// Exists purely to be patched into by RJW integration
        /// </summary>
        public override void WriteSettings()
        {
            base.WriteSettings();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            // the base games settings are extremely static, they have a fixed size and are not draggable
            CloseNativeSettings();
            // custom window "fixes" those issues
            Find.WindowStack.Add(new Window_Settings());
        }

        private void CloseNativeSettings()
        {
            Find.WindowStack.TryRemoveAssignableFromType(typeof(RimWorld.Dialog_ModSettings), false);
            Find.WindowStack.TryRemoveAssignableFromType(typeof(HugsLib.Settings.Dialog_ModSettings), false);
            Find.WindowStack.TryRemoveAssignableFromType(typeof(Dialog_VanillaModSettings), false);
        }
    }
}
