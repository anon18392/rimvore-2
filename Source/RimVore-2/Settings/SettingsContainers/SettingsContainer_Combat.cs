﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class SettingsContainer_Combat : SettingsContainer
    {
        public SettingsContainer_Combat() { }

        private FloatSmartSetting grappleVerbSelectionBaseChance;
        private FloatSmartSetting grappleVerbSelectionMaxChance;
        private FloatSmartSetting grappleStrengthMeleeSkillDivider;
        private BoolSmartSetting useAutoRules;
        private FloatSmartSetting defaultRequiredStruggles;
        private FloatSmartSetting defaultStruggleChance;
        private BoolSmartSetting grapplePawnsShareCell;
        private FloatSmartSetting grappleFailureStunDuration;

        public float GrappleVerbSelectionBaseChance => grappleVerbSelectionBaseChance.value / 100;
        public float GrappleVerbSelectionMaxChance => grappleVerbSelectionMaxChance.value / 100;
        public int GrappleStrengthMeleeSkillDivider => (int)grappleStrengthMeleeSkillDivider.value;
        public bool UseAutoRules => useAutoRules.value;
        public int DefaultRequiredStruggles => (int)defaultRequiredStruggles.value;
        public float DefaultStruggleChance => defaultStruggleChance.value / 100;
        public bool GrapplePawnsShareCell => grapplePawnsShareCell.value;
        public int GrappleFailureStunDuration => (int)grappleFailureStunDuration.value;

        public override void EnsureSmartSettingDefinition()
        {
            if(grappleVerbSelectionBaseChance == null || grappleVerbSelectionBaseChance.IsInvalid())
                grappleVerbSelectionBaseChance = new FloatSmartSetting("RV2_Settings_Combat_GrappleVerbSelectionBaseChance", 15f, 15f, 0, 100, "RV2_Settings_Combat_GrappleVerbSelectionBaseChance_Tip", "0", "%");
            if(grappleVerbSelectionMaxChance == null || grappleVerbSelectionMaxChance.IsInvalid())
                grappleVerbSelectionMaxChance = new FloatSmartSetting("RV2_Settings_Combat_GrappleVerbSelectionMaxChance", 50f, 50f, 0, 100, "RV2_Settings_Combat_GrappleVerbSelectionMaxChance_Tip", "0", "%");
            if(grappleStrengthMeleeSkillDivider == null || grappleStrengthMeleeSkillDivider.IsInvalid())
                grappleStrengthMeleeSkillDivider = new FloatSmartSetting("RV2_Settings_Combat_GrappleStrengthMeleeSkillDivider", 5f, 5f, 1, 20, "RV2_Settings_Combat_GrappleStrengthMeleeSkillDivider_Tip", "0");
            if(useAutoRules == null || useAutoRules.IsInvalid())
                useAutoRules = new BoolSmartSetting("RV2_Settings_Combat_UseAutoRules", true, true, "RV2_Settings_Combat_UseAutoRules_Tip");
            if(defaultRequiredStruggles == null || defaultRequiredStruggles.IsInvalid())
                defaultRequiredStruggles = new FloatSmartSetting("RV2_Settings_Combat_DefaultRequiredStruggles", 60f, 60f, 1, 500, "RV2_Settings_Combat_DefaultRequiredStruggles_Tip", "0");
            if(defaultStruggleChance == null || defaultStruggleChance.IsInvalid())
                defaultStruggleChance = new FloatSmartSetting("RV2_Settings_Combat_DefaultStruggleChance", 35f, 35f, 0, 100, "RV2_Settings_Combat_DefaultStruggleChance_Tip", "0", "%");
            if(grapplePawnsShareCell == null || grapplePawnsShareCell.IsInvalid())
                grapplePawnsShareCell = new BoolSmartSetting("RV2_Settings_Combat_GrapplePawnsShareCell", true, true, "RV2_Settings_Combat_GrapplePawnsShareCell_Tip");
            if(grappleFailureStunDuration == null || grappleFailureStunDuration.IsInvalid())
                grappleFailureStunDuration = new FloatSmartSetting("RV2_Settings_Combat_GrappleFailureStunDuration", 500f, 500f, 1, 1000, "RV2_Settings_Combat_GrappleFailureStunDuration_Tip");

        }

        public override void Reset()
        {
            grappleVerbSelectionBaseChance = null;
            grappleVerbSelectionMaxChance = null;
            grappleStrengthMeleeSkillDivider = null;
            useAutoRules = null;
            defaultRequiredStruggles = null;
            defaultStruggleChance = null;
            grapplePawnsShareCell = null;
            grappleFailureStunDuration = null;

            EnsureSmartSettingDefinition();
        }

        private bool heightStale = true;
        private float height = 0f;
        private Vector2 scrollPosition;
        public void FillRect(Rect inRect)
        {
            #region scrollViewStart
#if v1_2
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width,
                maxOneColumn = true
            };
            list.Begin(inRect);
            Rect outerRect = list.GetRect(inRect.height - list.CurHeight); ;
            list.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Rect innerRect);
#else
            Rect outerRect = inRect;
            UIUtility.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Listing_Standard list);
#endif
            #endregion

            if(list.ButtonText("RV2_Settings_Reset".Translate()))
                Reset();

            useAutoRules.DoSetting(list);

            if(RV2Mod.Settings.features.GrapplingEnabled)
            {
                grappleVerbSelectionBaseChance.DoSetting(list);
                if(grappleVerbSelectionBaseChance.value > grappleVerbSelectionMaxChance.value) // make sure max is always at least equal to base
                    grappleVerbSelectionMaxChance.value = grappleVerbSelectionBaseChance.value;
                grappleVerbSelectionMaxChance.DoSetting(list);
                grappleStrengthMeleeSkillDivider.DoSetting(list);
                grapplePawnsShareCell.DoSetting(list);
                grappleFailureStunDuration.DoSetting(list);
            }
            if(RV2Mod.Settings.features.StrugglingEnabled)
            {
                defaultRequiredStruggles.DoSetting(list);
                defaultStruggleChance.DoSetting(list);
            }

            #region scrollViewEnd
#if v1_2
            list.EndScrollView(ref height, ref heightStale, ref innerRect);
            list.End();
#else
            list.EndScrollView(ref height, ref heightStale);
#endif
            #endregion
        }

        public override void ExposeData()
        {
            if(Scribe.mode == LoadSaveMode.Saving || Scribe.mode == LoadSaveMode.LoadingVars)
            {
                EnsureSmartSettingDefinition();
            }
            Scribe_Deep.Look(ref grappleVerbSelectionBaseChance, "grappleVerbSelectionBaseChance", new object[0]);
            Scribe_Deep.Look(ref grappleVerbSelectionMaxChance, "grappleVerbSelectionMaxChance", new object[0]);
            Scribe_Deep.Look(ref grappleStrengthMeleeSkillDivider, "grappleStrengthMeleeSkillDivider", new object[0]);
            Scribe_Deep.Look(ref useAutoRules, "useAutoRules", new object[0]);
            Scribe_Deep.Look(ref defaultRequiredStruggles, "defaultRequiredStruggles", new object[0]);
            Scribe_Deep.Look(ref defaultStruggleChance, "defaultStruggleChance", new object[0]);
            Scribe_Deep.Look(ref grapplePawnsShareCell, "grapplePawnsShareCell", new object[0]);
            Scribe_Deep.Look(ref grappleFailureStunDuration, "grappleFailureStunDuration", new object[0]);

            PostExposeData();
        }
    }
}