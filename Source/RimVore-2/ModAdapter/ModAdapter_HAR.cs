﻿using Verse;
using System.Collections.Generic;
using System.Linq;
using AlienRace;

namespace RimVore2
{
    // HAR = Humanoid Animal Races
    public class ModAdapter_HAR
    {
        public IEnumerable<ThingDef_AlienRace> LoadedAlienRaces = DefDatabase<ThingDef_AlienRace>.AllDefsListForReading;
        public IEnumerable<ThingDef_AlienRace> SortedAlienRaces => LoadedAlienRaces.OrderBy(race => race.label);

        //TODO_Maybe pre-defined list of incompatible races (none so far)
    }
}
